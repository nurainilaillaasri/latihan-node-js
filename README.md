Membuat node js project dr 0, dengan npm init dan seterusnya
![1_awal](/uploads/31ff45a79f962aa3859676053fb6688a/1_awal.png)

Install dan import nodemon, express, ejs dan fs module ke project kalian
![4_install_nodemon](/uploads/dca6bcf23dda9541f8a3ce22478de76d/4_install_nodemon.png)

![5_install_epress_ejs_fs](/uploads/d01cd8013c3be9745c135122ef0fb23f/5_install_epress_ejs_fs.png)

Membuat script npm run dev yang melakukan nodemon index.js
![6_nodemon_index](/uploads/f0bc25fbe4ca24b2ecc7135f4807f147/6_nodemon_index.png)

// 4. bagian fs module, lakukan persis dengan yg di reading material, boleh kalau mau lbh kreatif
![7](/uploads/40a41317b0b7de70fcb36dacfae1ac0f/7.jpeg)

// 5. render halaman localhost:3000/data yg berisi list item dari person.json(dari fs module yg di reading material)
