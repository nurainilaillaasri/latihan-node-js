const fs = require('fs');
const express = require('express');

const isi = fs.readFileSync('./text.txt', 'utf-8');
console.log(isi);

fs.writeFileSync('./test.txt', "I love Binar");
const isi2 = fs.readFileSync('./test.txt', 'utf-8');
console.log(isi2);

// const data = require('./create.js');
const person = require("./person.json");
console.log(person);

// No 5
const app = express();

app.get('/', (req, res) => {
    res.render('index.ejs')
})

app.get('/person', (req, res) => {
    res.render('person.ejs', {
        data: person
    })
})

app.listen(3000);
