const fs = require('fs');

const createPerson = function (person) {
    fs.writeFileSync('./person.json', JSON.stringify(person));
    return person;
}

const lailla = createPerson({
    name: 'Lailla',
    age: 22,
    address: 'Karawang'
})

module.exports = createPerson;